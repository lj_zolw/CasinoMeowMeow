﻿using CasinoCore.Entities.Decks;
using CasinoCore.Usecases.GameActions;
using CasinoCore.Usecases.GameConditions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CasinoCore.Entities.GameStates;
using CasinoCore.Usecases.CardComparison;

namespace CasinoCore.Entities.Games.Guessing
{
    public interface IGameImplementation
    {
        string Title();
        CardDeck CardDeck();
        List<IGameAction> AvailableActions();
        List<IGameCondition> VictoryConditions();
        List<IGameCondition> GameStopConditions();
        GameState InitialGameState();
        ICardComparisonStrategy CardComparisonStrategy();
    }
}
