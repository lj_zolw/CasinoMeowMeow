﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CasinoCore.Containers.GameRules;
using CasinoCore.Entities.GameStates;
using CasinoCore.Entities.Cards;
using CasinoCore.Usecases.InfluenceState;
using CasinoCore.Entities.Decks;

namespace CasinoCore.Usecases.GameActions
{
    public class Add10COrDrawACard : IGameAction
    {
        public void ChangeGameState(GameState currentGameState, PlayedGameRules gameRules, string orderParams)
        {
            CardDeck deck = QueryGameState.ExtractCardDeck(currentGameState);

            if (QueryGameState.AmountOfCardsLeft(currentGameState) % 2 == 0)
            {    
                deck.AddASingleCard(new Card("10", "C"));
            }
            else
            {
                deck.DrawRandomCard();
            }
        }

        public bool ShouldReactTo(string item1)
        {
            throw new NotImplementedException();
        }
    }
}
