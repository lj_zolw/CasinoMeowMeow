﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CasinoCore.Entities.Cards;

namespace CasinoCore.Usecases.CardComparison
{
    public class RankCardComparisonStrategy : ICardComparisonStrategy
    {
        public bool AreTheSame(Card card1, Card card2)
        {
            if (card1.Rank() == card2.Rank())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
