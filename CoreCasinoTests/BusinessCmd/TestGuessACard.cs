﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CasinoCore.Entities.Decks;
using System.Collections.Generic;
using CasinoCore.Interfaces.GameManagers;
using CasinoCore.Entities.GameStates;
using CasinoCore.Entities.Cards;
using CasinoCore.Usecases.InfluenceState;

namespace CoreCasinoTests
{
    [TestClass]
    public class TestGuessACard
    {

        [TestMethod]
        public void DirectCardGuessingWorks__ForThreeCardsDeck__Failure()
        {
            // Given primitives
            var cards = new List<string> { "JH", "QH", "KH" };
            var cardOutsideDeck = "2C";
            var secondSeparator = ",";
            string cardsInDeck = string.Join(secondSeparator, cards);

            // Given
            string orders = "Play GuessACard; SetDeck " + cardsInDeck + "; SetMaxTurns 3; " +
                "Guess " + cardOutsideDeck + "; Guess " + cardOutsideDeck + "; Guess " + cardOutsideDeck;

            GameManager gm = new CreateGameManager().DefaultsWithOrders(orders);

            // When
            gm.ExecuteOrders(orders);
            GameState resultReceived = gm.CurrentState();

            // Then
            Assert.IsTrue(QueryGameState.GameUndergoing(resultReceived) == "Defeat");
            Assert.IsTrue(QueryGameState.MaximumTurns(resultReceived) == QueryGameState.CurrentTurn(resultReceived));
            Assert.IsTrue(QueryGameState.AmountOfCardsLeft(resultReceived) == 0);

        }

        [TestMethod]
        public void DirectCardGuessingWorks__ForThreeCardsDeck__Failure_DiffSeparators()
        {
            // Given primitives
            var cards = new List<string> { "JH", "QH", "KH" };
            var cardOutsideDeck = "2C";
            var secondSeparator = ";";
            string cardsInDeck = string.Join(secondSeparator, cards);

            // Given
            string orders = "Play GuessACard, SetDeck " + cardsInDeck  + ", SetMaxTurns 3, " + 
                "Guess " + cardOutsideDeck + ", Guess " + cardOutsideDeck + ", Guess " + cardOutsideDeck;

            GameManager gm = new CreateGameManager().DefaultsWithOrders(orders);

            // When
            gm.ExecuteOrders(orders);
            GameState resultReceived = gm.CurrentState();

            // Then
            Assert.IsTrue(QueryGameState.GameUndergoing(resultReceived) == "Defeat");
            Assert.IsTrue(QueryGameState.MaximumTurns(resultReceived) == QueryGameState.CurrentTurn(resultReceived));
            Assert.IsTrue(QueryGameState.AmountOfCardsLeft(resultReceived) == 0);

        }

        [TestMethod]
        public void DirectCardGuessingWorks__ForThreeCardsDeck__Victory()
        {
            // Given primitives
            var cards = new List<string> { "JH", "QH", "KH" };
            var cardInsideDeck = "JH";
            var secondSeparator = ",";
            string cardsInDeck = string.Join(secondSeparator, cards);

            // Given
            string orders = "Play GuessACard; SetDeck " + cardsInDeck + "; SetMaxTurns 3; " +
                "Guess " + cardInsideDeck + "; Guess " + cardInsideDeck + "; Guess " + cardInsideDeck;

            GameManager gm = new CreateGameManager().DefaultsWithOrders(orders);

            // When
            gm.ExecuteOrders(orders);
            GameState resultReceived = gm.CurrentState();

            // Then
            Assert.IsTrue(QueryGameState.GameUndergoing(resultReceived) == "Victory");

        }

        [TestMethod]
        public void CardRedBlackGuessingWorks__ForThreeCardsOneColourDeck_MissMissVictory()
        {
            // Given primitives
            var cards = new List<string> { "JH", "QH", "KH" };
            var cardOutsideDeck = "2C";
            var cardInsideDeck = "2D";
            var secondSeparator = ",";
            string cardsInDeck = string.Join(secondSeparator, cards);

            // Given
            string orders = "Play GuessACard; SetDeck " + cardsInDeck + "; GameVariant RedBlack; " +
                "Guess " + cardOutsideDeck + "; Guess " + cardOutsideDeck + "; Guess " + cardInsideDeck;

            GameManager gm = new CreateGameManager().DefaultsWithOrders(orders);

            // When
            gm.ExecuteOrders(orders);
            GameState resultReceived = gm.CurrentState();

            // Then
            Assert.IsTrue(QueryGameState.GameUndergoing(resultReceived) == "Victory", "We received: " + 
                QueryGameState.GameUndergoing(resultReceived));

        }

    }
}
