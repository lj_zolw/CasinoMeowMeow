﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CasinoCore.Entities.GameStates;

namespace CasinoCore.Usecases.GameConditions
{
    public interface IGameCondition
    {
        void CheckAndUpdate(GameState currentGameState);
    }
}
