﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CasinoCore.Containers.GameRules;
using CasinoCore.Entities.GameStates;
using CasinoCore.Usecases.InfluenceState;

namespace CasinoCore.Usecases.GameActions
{
    public class DrawSingleCardAction : IGameAction
    {
        public void ChangeGameState(GameState currentGameState, PlayedGameRules gameRules, string orderParams)
        {
            QueryGameState.ExtractCardDeck(currentGameState).DrawRandomCard();

            if (QueryGameState.AmountOfCardsLeft(currentGameState) == 0)
                currentGameState["Guess"] = true;
        }

        public bool ShouldReactTo(string item1)
        {
            throw new NotImplementedException();
        }
    }
}
