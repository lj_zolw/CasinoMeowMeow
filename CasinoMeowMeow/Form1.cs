﻿using CasinoCore.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CasinoMeowMeow
{
    public partial class Form1 : Form
    {
        private UiLogicMediator _mediator;

        public Form1()
        {
            InitializeComponent();
            _mediator = new UiLogicMediator();
        }

        private void btnDoStuff_Click(object sender, EventArgs e)
        {
            _mediator.DoStuff(txtInput.Text);
            txtDisplay.Text = _mediator.DisplayCurrentState();
        }

        private void btnSetGameParams_Click(object sender, EventArgs e)
        {
            _mediator.SetupGame(txtInput.Text);
            txtDisplay.Text = _mediator.DisplayCurrentState();
        }
    }
}
