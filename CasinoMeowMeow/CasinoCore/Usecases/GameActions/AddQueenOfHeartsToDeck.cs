﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CasinoCore.Containers.GameRules;
using CasinoCore.Entities.GameStates;
using CasinoCore.Entities.Cards;
using CasinoCore.Entities.Decks;
using CasinoCore.Usecases.InfluenceState;

namespace CasinoCore.Usecases.GameActions
{
    public class AddQueenOfHeartsToDeck : IGameAction
    {
        public void ChangeGameState(GameState currentGameState, PlayedGameRules gameRules, string orderParams)
        {
            CardDeck deck = QueryGameState.ExtractCardDeck(currentGameState);
            deck.AddASingleCard(new Card("Q", "H"));
        }

        public bool ShouldReactTo(string item1)
        {
            throw new NotImplementedException();
        }
    }
}
