﻿using CasinoCore.Containers.GameManagers.Rules.CreationCommands;
using CasinoCore.Containers.GameRules.CreationCommands;
using CasinoCore.Usecases.GameActions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasinoCore.Containers.GameRules
{
    public class CreateGameStructure
    {
        List<ICreateGameRulesCommand> _commands; 

        public CreateGameStructure()
        {
            _commands = RegisterAvailableCreationCommands();
        }
        

        public GameManagerInternalsBuilder BuildRulesAndInitialState(List<Tuple<string, string>> orders)
        {
            GameManagerInternalsBuilder builder = new GameManagerInternalsBuilder();
            foreach (var order in orders)
            {
                foreach (var orderUser in _commands)
                {
                    if(orderUser.ShouldReactTo(order.Item1))
                    {
                        orderUser.ChangeGameRuleset(builder, order.Item2);
                    }
                }
            }

            return builder;
        }


        private List<ICreateGameRulesCommand> RegisterAvailableCreationCommands()
        {
            return new List<ICreateGameRulesCommand>() { new SelectGame(), new SetDeck(), new SetMaxTurns(),
                new GameVariant() };
        }
    }
}
