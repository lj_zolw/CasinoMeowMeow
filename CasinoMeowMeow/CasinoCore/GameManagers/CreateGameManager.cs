﻿using CasinoCore.Containers.GameRules;
using CasinoCore.Entities.GameStates;
using CasinoCore.GameManagers.SelectedRules;
using CasinoCore.Technical.Parsers;
using CasinoCore.Usecases.CardComparison;
using CasinoCore.Usecases.GameActions;
using CasinoCore.Usecases.GameConditions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasinoCore.Interfaces.GameManagers
{
    public class CreateGameManager
    {
        public GameManager DefaultsWithOrders(string orderString)
        {
            List<Tuple<string, string>> orders = new SelectedParser().ProperlyParse(orderString);

            GameManagerInternalsBuilder builder = new CreateGameStructure().BuildRulesAndInitialState(orders);

            PlayedGameRules rules = builder.ConstructRuleset();
            GameState initialState = builder.ConstructGameState();

            return new GameManager(initialState, rules);
        }

        public GameManager Empty()
        {
            GameState initialState = new CreateGameState().Default();
            PlayedGameRules rules = new CreatePlayedGameRules().Empty();

            return new GameManager(initialState, rules);
        }
    }
}
