﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CasinoCore.Containers.GameRules;
using CasinoCore.Entities.GameStates;

namespace CasinoCore.Usecases.GameActions
{
    public interface IGameAction
    {
        bool ShouldReactTo(string item1);
        void ChangeGameState(GameState currentGameState, PlayedGameRules gameRules, string orderParams);
    }
}
