﻿using CasinoCore.Containers.GameRules.CreationCommands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CasinoCore.Containers.GameRules;
using CasinoCore.Entities.Decks;
using CasinoCore.Entities.Cards;
using CasinoCore.Usecases.CardComparison;

namespace CasinoCore.Containers.GameManagers.Rules.CreationCommands
{
    public class GameVariant : ICreateGameRulesCommand
    {
        private string _identifier = "GameVariant";

        public void ChangeGameRuleset(GameManagerInternalsBuilder builder, string parameters)
        {
            // First, the comparator
            builder.SetCardComparisonStrategy(new BlackRedColourOnlyComparisonStrategy());
        }

        public bool ShouldReactTo(string outerCommandName)
        {
            return _identifier.ToLower() == outerCommandName.ToLower();
        }
    }
}
